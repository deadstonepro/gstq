FROM alpine:latest

COPY . /gstq
WORKDIR /gstq

# Setting up the environment
RUN apk add --no-cache --update python3 
RUN python3 -m ensurepip
RUN python3 -m pip install --upgrade pip
RUN apk add --no-cache --update gcc libc-dev python3-dev musl-dev linux-headers
RUN python3 -m pip install -r ./requirements.txt
RUN apk del gcc libc-dev python3-dev musl-dev linux-headers

# Setting up nginx
RUN apk add --no-cache --update nginx
RUN mkdir /run/nginx
RUN rm /etc/nginx/conf.d/default.conf
COPY deploy/gstq.conf /etc/nginx/conf.d/

ENV RECAPTCHA_THRESHOLD=0.7
ENV FLASK_ENV=production
RUN mkdir var
RUN mkdir /var/gstq
CMD nginx && uwsgi --ini deploy/uwsgi.ini
