""" 
Benchmark for gstq project.
Uses single thread and single token.
Last update: 2020-10-04
"""

import time
import hashlib
import requests
from argparse import ArgumentParser


def push_request(host: str, token: str, data: str) -> float:
    """ Returns amount of time """
    endpoint = "http://" + host + "/api/push"
    headers = {
        "Content-Type": "text/plain",
        token: token
    }
    begin = time.time()
    requests.post(endpoint, headers=headers, data=data)
    finish = time.time()
    return finish - begin


def pop_request(host: str, token: str) -> float:
    """ Returns amount of time """
    endpoint = "http://" + host + "/api/pop"
    headers = { token: token }
    begin = time.time()
    requests.post(endpoint, headers=headers)
    finish = time.time()
    return finish - begin


def get_parser() -> ArgumentParser:
    parser = ArgumentParser(description="Single thread single token benchamrk.")
    parser.add_argument("token")
    parser.add_argument("--host", default="gstq.gtorianik.com")
    parser.add_argument("--requests", type=int, default=128)
    parser.add_argument("--data-size", type=int, default=64)
    return parser


def self_info() -> (str, str):
    """ Returns filename and md5 checksum of the current source file. """
    with open(__file__, "r") as fsrc:
        src = fsrc.read()
    checksum = hashlib.md5(__file__.encode("utf-8")).hexdigest()
    return __file__, checksum


if __name__ == "__main__":
    args = get_parser().parse_args()
    measures = list()
    for _ in range(args.requests):
        push_time = push_request(args.host, args.token, "0" * args.data_size)
        pop_time = pop_request(args.host, args.token)
        measures.append(push_time + pop_time)
    
    benchmark, checksum = self_info()
    print("Benchmark:", benchmark)
    print("MD5 checksum:", checksum)
    print("Host:", args.host)
    print("Number of requests:", args.requests)
    print("Request size(in bytes):", args.data_size)
    print("Overall time:", round(sum(measures), 4))
    print("Average time per request:", round(sum(measures) / len(measures), 4))
