import os
import json
import shutil
import functools
import tempfile
import unittest

from gstq import get_app

class TestRoutes(unittest.TestCase):
    test_folder = "./test_var"
    db_file = os.path.join(test_folder, "test.db")

    def setUp(self):
        try:
            os.mkdir(self.test_folder)
        except FileExistsError:
            shutil.rmtree(self.test_folder)
            os.mkdir(self.test_folder)
        self.app = get_app(DATABASE=self.db_file, TESTING=True)

    def tearDown(self):
        shutil.rmtree(self.test_folder)

    def flask_context(foo):
        @functools.wraps(foo)
        def wrapper(self, *args, **kwargs):
            with self.app.test_client() as client:
                return foo(self, client, *args, **kwargs)
        return wrapper

    @flask_context
    def test_create(self, client):
        resp = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"})
        self.assertEqual(resp.status, "200 OK")
        self.assertEqual(resp.headers.get("Content-Type"), "application/json")
        self.assertEqual(resp.json.get("success"), True)
        self.assertTrue(resp.json.get("token") != None)

    @flask_context
    def test_push(self, client):
        token = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"}).json["token"]
        resp = client.post("/api/push",
                data="Test string",
                headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        self.assertEqual(resp.status, "200 OK")
        self.assertEqual(resp.json.get("success"), True)

    @flask_context
    def test_push_wrong_token(self, client):
        token = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"}).json["token"]
        resp = client.post("/api/push",
                data="",
                headers={
                    "Content-Type": "text/plain",
                    "Token": "WRONG" # I was burn in the wrong house...
                })
        self.assertEqual(resp.status, "400 BAD REQUEST")
        self.assertEqual(resp.json.get("success"), False)
        self.assertEqual(resp.json.get("message"), "Couldn't extract the " \
                "data from the request. Try to add a header with mime-type.")

    @flask_context
    def test_pop(self, client):
        item_text = "Lorem ipsum dolor sit amet."
        token = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"}).json["token"]
        client.post("/api/push",
                data=item_text,
                headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        resp = client.post("/api/pop", headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        self.assertEqual(resp.status, "200 OK")
        self.assertEqual(resp.json.get("success"), True)
        self.assertEqual(resp.json.get("data"), item_text)

    @flask_context
    def test_pop_empty(self, client):
        token = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"}).json["token"]
        resp = client.post("/api/pop", headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        self.assertEqual(resp.status, "400 BAD REQUEST")
        self.assertEqual(resp.json.get("success"), False)
        self.assertEqual(resp.json.get("message"), "Queue is empty")

    @flask_context
    def test_push_pop_multiple(self, client):
        item1 = "Lorem ipsum"
        item2 = "Dolor sit"
        item3 = "Amet."
        token = client.post("/api/request_token", 
                data=json.dumps({"recaptcha_token": "MOCK"}), 
                headers={"Content-Type": "application/json"}).json["token"]
        client.post("/api/push",
                data=item1,
                headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        self.assertEqual(
            client.post("/api/pop", headers={"Token": token}).json.get("data"),
            item1)
        self.assertEqual(
            client.post("/api/pop", headers={"Token": token}).json.get("success"),
            False)
        client.post("/api/push",
                data=item2,
                headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        client.post("/api/push",
                data=item3,
                headers={
                    "Content-Type": "text/plain",
                    "Token": token
                })
        self.assertEqual(
            client.post("/api/pop", headers={"Token": token}).json.get("data"),
            item2)
        self.assertEqual(
            client.post("/api/pop", headers={"Token": token}).json.get("data"),
            item3)
