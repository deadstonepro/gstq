import os
import shutil
import unittest
import subprocess

from gstq.queue import QueueIsEmpty, QueueDoesNotExist
from gstq.queue_impls import SQLiteQueue

class SQLiteQueueTest(unittest.TestCase):

    test_folder = "./test_var"
    db_file = os.path.join(test_folder, "test.db")

    def setUp(self):
        try:
            os.mkdir(SQLiteQueueTest.test_folder)
        except FileExistsError:
            shutil.rmtree(SQLiteQueueTest.test_folder)
            os.mkdir(SQLiteQueueTest.test_folder)
        self.queue = SQLiteQueue(SQLiteQueueTest.db_file)
        self.queue.init()

    def tearDown(self):
        shutil.rmtree(SQLiteQueueTest.test_folder)

    def test_push_pop_size(self):
        t = "TOKEN"
        self.queue.alloc(t)
        self.queue.push(t, "123")
        self.assertEqual(self.queue.size(t), 1)
        self.queue.pop(t)
        self.assertEqual(self.queue.size(t), 0)
        self.queue.push(t, "123")
        self.assertEqual(self.queue.size(t), 1)
        self.queue.push(t, "456")
        self.assertEqual(self.queue.size(t), 2)
        self.queue.pop(t)
        self.assertEqual(self.queue.size(t), 1)
        self.queue.pop(t)
        self.assertEqual(self.queue.size(t), 0)

    def test_no_alloc(self):
        t = "TOKEN"
        with self.assertRaises(QueueDoesNotExist):
            self.queue.push(t, "123")
        with self.assertRaises(QueueDoesNotExist):
            self.queue.pop(t)
        with self.assertRaises(QueueDoesNotExist):
            self.queue.size(t)
        with self.assertRaises(QueueDoesNotExist):
            self.queue.dealloc(t)

    def test_late_alloc(self):
        t = "TOKEN"
        with self.assertRaises(QueueDoesNotExist):
            self.queue.push(t, "123")
        self.queue.alloc(t)
        self.queue.push(t, "123")
        self.queue.pop(t)
        self.queue.size(t)
        self.queue.dealloc(t)

    def test_dealloc(self):
        t = "TOKEN"
        self.queue.alloc(t)
        self.queue.push(t, "123")
        self.queue.pop(t)
        self.queue.size(t)
        self.queue.dealloc(t)
        with self.assertRaises(QueueDoesNotExist):
            self.queue.push(t, "123")

    def test_push_back_order(self):
        t = "TOKEN"
        vs = ["abc", "def", "julia"]
        rs = []
        self.queue.alloc(t)
        self.queue.push(t, vs[0])
        self.queue.push(t, vs[1])
        rs.append(self.queue.pop(t))
        self.queue.push(t, vs[2])
        rs.append(self.queue.pop(t))
        rs.append(self.queue.pop(t))
        self.assertEqual(vs, rs)

    def test_pop_empty(self):
        t = "TOKEN"
        self.queue.alloc(t)
        with self.assertRaises(QueueIsEmpty):
            self.queue.pop(t)
        self.queue.push(t, "")
        self.queue.pop(t)
        with self.assertRaises(QueueIsEmpty):
            self.queue.pop(t)

    def test_str_bytes(self):
        t = "TOKEN"
        vs = ["hello", b"world", b"julia", "is hot"]
        rs = []
        expected = ["hello", "world", "julia", "is hot"]
        self.queue.alloc(t)
        for v in vs:
            self.queue.push(t, v)
            rs.append(self.queue.pop(t))
        self.assertEqual(rs, expected)

    def test_push_pop_mult(self):
        t1 = "TOKEN1"
        t2 = "TOKEN2"
        vs1 = ["hello", "world", "julia", "is hot"]
        rs1 = []
        vs2 = ["buy", "george", "julia isn't", "for ya ;)"]
        rs2 = []
        self.queue.alloc(t1)
        self.queue.alloc(t2)
        for (v1, v2) in zip(vs1, vs2):
            self.queue.push(t1, v1)
            self.queue.push(t2, v2)
            rs1.append(self.queue.pop(t1))    
            rs2.append(self.queue.pop(t2))    
        self.assertEqual(rs1, vs1)
        self.assertEqual(rs2, vs2)

    def test_alloc_dealloc_mult(self):
        t1 = "TOKEN1"
        t2 = "TOKEN2"
        self.queue.alloc(t1)
        with self.assertRaises(QueueDoesNotExist):
            self.queue.push(t2, "DATA")
        self.queue.alloc(t2)
        self.queue.dealloc(t1)
        with self.assertRaises(QueueDoesNotExist):
            self.queue.push(t1, "DATA")
