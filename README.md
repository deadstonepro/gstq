## God safe the queue 

### Deploy
To run the docker container:
```bash
docker run -d \
    -e SERVER_NAME=... \
    -e RECAPTCHA_SITE_KEY=... \
    -e RECAPTCHA_SECRET_KEY=... \
    --name gstq \
    -p 80:80 \ 
    gtorianik/gstq:<version>
```

### Benchmark
#### Version 0.1.1
Machine specs: 1 shared CPU
```
Benchmark: benchmarks/st2.py
MD5 checksum: 3628a387074fabff18cd4f23ff4e24c1
Host: gstq.gtorianik.com
Number of requests: 128
Request size(in bytes): 64
Overall time: 34.6352
Average time per request: 0.2706
```
