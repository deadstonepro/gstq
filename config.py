import os

class Config:
    SERVER_NAME = os.environ.get("SERVER_NAME")
    QUEUE = "gstq.queue_impls.SQLiteQueue"
    RECAPTCHA_SITE_KEY = os.environ["RECAPTCHA_SITE_KEY"]
    RECAPTCHA_SECRET_KEY = os.environ["RECAPTCHA_SECRET_KEY"]
    RECAPTCHA_SCORE_THRESHOLD = float(os.environ.get("RECAPTCHA_THRESHOLD", 0.9))
    DEBUG = False
    TESTING = False

class DevelopmentConfig(Config):
    DATABASE = "./var/db.devel.sqlite"
    DEBUG = True

class ProductionConfig(Config):
    DATABASE = "./var/db.prod.sqlite"
