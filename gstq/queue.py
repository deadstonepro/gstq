from typing import Union

class QueueIsEmpty(Exception):
    pass


class QueueDoesNotExist(Exception):
    pass


class QueueAlreadyExists(Exception):
    pass


class AQueue:
    """ Abstract interface to the queue. 
        
        Queue implementation must guarantees that all
        requests will be processed in the same order 
        as they were called.
    """

    def init(self):
        """ Prepares the queue-server, 
        fill it with a necessary data. """
        pass

    def alloc(self, token):
        """ Create a single queue with a given token. 

        Raises QueueAlreadyExists. """
        pass

    def dealloc(self, token):
        """ Remove the queue with the given token.

        Raises QueueDoesNotExist. """
        pass

    def size(self, token):
        """ Returns number of elements in the queue. """
        pass

    def push(self, token, data: Union[str, bytes]):
        """ Converts data into the utf-8 string and push it
        into the end of the queue.

        Raises QueueDoesNotExist. """
        pass

    def pop(self, token):
        """ Exctracts and deletes first element in the queue.

        Raises QueueDoesNotExist, QueueIsEmpty. """
        pass

    def init_app(self, flask_app):
        """ Flask adapter. """
        setattr(flask_app, "queue", self)
        setattr(flask_app, "init_queue", self.init)
