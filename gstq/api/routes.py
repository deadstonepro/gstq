import random
import string

from flask import Blueprint, current_app, request, jsonify

from .utils import recaptcha

from gstq.queue import QueueDoesNotExist, QueueIsEmpty

api_blueprint = Blueprint("api", __name__)

@api_blueprint.route('/request_token', methods=["POST"])
@recaptcha
def request_token():
    alphabet = string.ascii_uppercase + string.digits
    token = ''.join(random.choice(alphabet) for i in range(8))
    current_app.queue.alloc(token)
    return jsonify({
        "success": True,
        "token": token
    }), 200


@api_blueprint.route('/push', methods=["POST"])
def push():
    if not request.data:
        return jsonify({
            "success": False,
            "message": "Couldn't extract the data from the request. " \
                    "Try to add a header with mime-type."
        }), 400
    if not (token := request.headers.get("Token")):
        return jsonify({
            "success": False,
            "message": "You must provide a token in order to " \
                    "modify the queue."
        }), 400
    try:
        current_app.queue.push(token, request.data)
    except QueueDoesNotExist:
        return jsonify({
            "success": False,
            "message": "Wrong token."
        }), 400
    return jsonify({
        "success": True,
    }), 200


@api_blueprint.route('/pop', methods=["POST"])
def pop():
    if not (token := request.headers.get("Token")):
        return jsonify({
            "success": False,
            "message": "You must provide a token in order to " \
                    "modify the queue."
        }), 400
    try:
        data = current_app.queue.pop(token)
    except QueueIsEmpty:
        return jsonify({
            "success": False,
            "message": "Queue is empty"
        }), 400
    return jsonify({
        "success": True,
        "data": data
    })
