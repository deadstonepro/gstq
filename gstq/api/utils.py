import functools

from flask import current_app, request, jsonify
import requests

def recaptcha(foo):
    @functools.wraps(foo)
    def wrapper(*args, **kwargs):
        recaptcha_verify_url = "https://www.google.com/recaptcha/api/siteverify"
        resp = requests.post(recaptcha_verify_url, params={
            "secret": current_app.config["RECAPTCHA_SECRET_KEY"],
            "response": request.json.get("recaptcha_token"),
            "remoteip": request.remote_addr
        }).json();
        threshold = current_app.config["RECAPTCHA_SCORE_THRESHOLD"]
        if not current_app.config["TESTING"] and (not resp["success"] or resp["score"] < threshold):
            return jsonify({
                "success": False,
                "message": "Suspicious activity."
            }), 400
        else:
            return foo(*args, **kwargs)
    return wrapper
