// Requires RECAPTCHA_SITE_KEY to be defined.

function main(event) {
    const req_token_handler = recaptcha_action(request_token, "request_token");

    document
        .getElementById("token-button")
        .addEventListener("click", req_token_handler);
}

function recaptcha_action(handler, action) {
    // Wraps existing function this way so recaptcha token 
    // will be extracted from the page and passed as last argument.
    return function(event) {
        event.preventDefault();
        grecaptcha.ready(
            () => grecaptcha
                .execute(RECAPTCHA_SITE_KEY, {'action': action})
                .then(token => handler(event, token))
        );
    }
}

function request_token(event, recaptcha_token) {
    fetch('api/request_token', {
        method: 'POST',
        headers: { 
            'Content-Type': 'application/json;charset=utf-8',
            'Recapctha-Token': recaptcha_token,
        },
        body: JSON.stringify({
            'recaptcha_token': recaptcha_token, // deprecated
        }),
    })
    .then(resp => resp.json())
    .then(resp => {
        document.getElementById("token-input").value = resp.token;
    });
}
