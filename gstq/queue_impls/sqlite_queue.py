import sqlite3
import functools

import flask

from gstq.queue import *

class SQLiteQueue(AQueue):
    _db_path_cfg_field = "DATABASE"

    _db_schema = """
CREATE TABLE queues (
queue_id INTEGER NOT NULL,
token TEXT NOT NULL,
PRIMARY KEY(queue_id));

CREATE TABLE items (
item_id INTEGER NOT NULL,
queue_id INTEGER NOT NULL,
data TEXT NOT NULL,
PRIMARY KEY(item_id),
FOREIGN KEY(queue_id) REFERENCES queues(queue_id));
"""
    def __init__(self, db_url=None):
        self._conn = None
        self._db_url = db_url

    def init_app(self, flask_app):
        super().init_app(flask_app)
        self._db_url = flask_app.config.get(SQLiteQueue._db_path_cfg_field) \
                or self._db_url

    def _transaction(func):
        @functools.wraps(func)
        def wrapper(self, *args, **kwargs):
            self._conn = sqlite3.connect(self._db_url)
            try:
                result = func(self, *args, **kwargs)
            except:
                self._conn.rollback()
                raise
            else:
                self._conn.commit()
            finally:
                self._conn.close()
                self._conn = None
            return result
        return wrapper

    @_transaction
    def init(self):
        try:
            self._conn.cursor().executescript(SQLiteQueue._db_schema)
        except sqlite3.OperationalError as exc:
            if "table queues already exists" not in exc.args:
                raise

    @_transaction
    def alloc(self, token: str):
        try:
            self._get_queue_id(token)
        except QueueDoesNotExist:
            self \
                ._conn.cursor() \
                .execute("INSERT INTO queues(token) VALUES (?) ;", 
                         (token,))
        else:
            raise QueueAlreadyExists()

    @_transaction
    def dealloc(self, token: str):
        qid = self._get_queue_id(token)
        self \
                ._conn.cursor() \
                .execute("DELETE FROM queues WHERE queue_id=?", (qid,))

    @_transaction
    def size(self, token: str) -> int:
        qid = self._get_queue_id(token)
        return len(
            self._conn.cursor() \
            .execute("SELECT (item_id) FROM items WHERE queue_id=?", (qid,)) \
            .fetchall())

    @_transaction
    def push(self, token: str, data):
        if isinstance(data, bytes):
            data = data.decode("utf-8")
        if not isinstance(data, str):
            raise TypeError("Data must be either a string or a bytes")
        qid = self._get_queue_id(token)
        self._conn.cursor() \
            .execute("INSERT INTO items(queue_id, data) VALUES (?, ?)",
                     (qid, data))

    @_transaction
    def pop(self, token: str) -> bytes:
        qid = self._get_queue_id(token)
        resp = self \
                ._conn.cursor() \
                .execute("SELECT item_id, data FROM items " \
                    "WHERE queue_id=? LIMIT 1 ;", (qid,)) \
                .fetchone()
        if not resp:
            raise QueueIsEmpty()
        last_item_id, data_str = resp
        self \
                ._conn.cursor() \
                .execute("DELETE FROM items WHERE item_id=?", (last_item_id,))
        return data_str

    def _get_queue_id(self, token: str) -> int:
        cols = self._conn.cursor().execute("SELECT (queue_id) FROM queues " \
                "WHERE token=? LIMIT 1 ;", (token,)).fetchone()
        if cols:
            qid, *_ = cols
            return int(qid)
        else:
            raise QueueDoesNotExist()
