import os
import importlib

from flask import Flask, Blueprint, current_app, render_template

from gstq.api import api_blueprint

web_blueprint = Blueprint("web", __name__)

@web_blueprint.route('/', methods=["GET"])
def index():
    return render_template("index.html", **current_app.config)

env_to_cfg = {
    "production": "config.ProductionConfig",
    "development": "config.DevelopmentConfig",
    "tesing": "config.TestingConfig",
}

def _load_object(path: str):
    *module_path, obj_name = path.split(".")
    module = importlib.import_module(".".join(module_path))
    return getattr(module, obj_name)

def get_app(**kwargs):
    app = Flask(__name__)
    app.config.from_object(env_to_cfg[app.config["ENV"]])
    app.config.update(**kwargs)
    app.register_blueprint(web_blueprint)
    app.register_blueprint(api_blueprint, url_prefix="/api")
    queue = _load_object(app.config["QUEUE"])()
    queue.init_app(app)
    app.init_queue()
    return app

if __name__ == "__main__":
    get_app().run()
